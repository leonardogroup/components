<?php

class ExampleForm extends Component {
  public $name = 'ExampleForm';
  public $key = 'Example_Form';
  public $label = 'Example Form';

  public function config(): void {
    $this->addSharedCss('forms');
  }
}
