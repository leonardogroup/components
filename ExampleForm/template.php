<?php
$component->setTag('section');
$component->addClass('example-form');
$component->start();
?>
<div class="content">
  <?php
    global $contactForm;
    $contactForm->render();
  ?>
</div>
<?php
$component->end(); ?>