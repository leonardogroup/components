<?php

class AdminMenu extends Component {
  public $name = 'AdminMenu';
  public $key = 'Admin_Menu';
  public $label = 'Admin Menu';

  public function config(): void {
    $this->setAdminVisibility(false);
  }

  public function getStyle(): string {
    $style_path = $this->path.'/module.css';
    if ( current_user_can('administrator') ) {
      return file_exists($style_path)
        ? file_get_contents($style_path)
        : ''
      ;
    } else {
      return '';
    }
  }
}
