<?php
if ( current_user_can('administrator') ):
$component->setId('wp-admin-menu');
$component->start();?>
<ul>
  <li>
    <a title="Dashboard" href="<?= get_dashboard_url(); ?>" class="logo">
      <span class="ico ico-wp-wp"></span>
    </a>
  </li>
  <li>
    <a title="Edit" href="<?= get_edit_post_link(); ?>" class="link edit">
      <span class="ico ico-wp-edit"></span>
    </a>
  </li>
  <li>
    <a title="Personalize" href="<?= get_dashboard_url(); ?>customize.php" class="link hidden-item personalize">
      <span class="ico ico-wp-personalize"></span>
    </a>
  </li>
  <li>
    <a title="Logout" href="<?= wp_logout_url(get_home_url()); ?>" class="link hidden-item logout">
      <span class="ico ico-wp-logout"></span>
    </a>
  </li>
</ul>
<?php 
$component->end();
endif;
?>