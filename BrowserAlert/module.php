<?php

class BrowserAlert extends Component {
  public $name = 'BrowserAlert';
  public $key = 'Browser_Alert';
  public $label = '⚠️ Browser Alert';

  
  public function config(): void {
    $this->setAdminVisibility(false);
  }
}
