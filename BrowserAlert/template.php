<?php 
$component->setId('browser-alert');
$component->start();?>
<div class="text-wrapper typo">
  <p class="h1"><?php trans('Uwaga!');?></p>
  <p><?php trans('<strong>Przeglądarka internetowa</strong>, z której korzystasz <strong>jest przestarzała</strong>, przez co strona <strong>nie będzie</strong> wyświetlać się w prawidłowy sposób.');?></p>
  <p class="small"><?php trans('Wymagana zmiana przeglądarki na np.: Chrome, Firefox, Opera');?></p>
</div>
<?php $component->end();?>