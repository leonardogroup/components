<?php

class CookieNotify extends Component {
  public $name = 'CookieNotify';
  public $key = 'Cookie_notify';
  public $label = '🍪 Cookie notify';

  
  public function config(): void {
    $this->addSharedJs('cookie');
    $this->setAdminVisibility(false);
  }
}
