var cookieNotify = Cookies.get('cookieNotify');

if ( cookieNotify == null ) {
  setTimeout(function(){
    document.getElementById('cookie-notify').classList.add('show');
  }, 300);
}

document.getElementById('accept-cookies').addEventListener('click', function(e){
  e.preventDefault();
  document.getElementById('cookie-notify').classList.add('fadeOff');
  Cookies.set('cookieNotify', false, { expires: 7, path: '/'  });
});
