<?php 
$component->setId('cookie-notify');
$component->start();?>
<div class="text typo">
  <p><?php trans('Na swoich stronach wykorzystujemy pliki cookies. Korzystając z naszych stron bez zmiany ustawień przeglądarki będą one zapisane w pamięci urządzenia.')?></p>
  <a href="#" id="accept-cookies"><?php trans('Ok');?></a>
  <a href="<?=get_privacy_policy_url();?>"><?php trans('Więcej');?></a>
</div>
<?php $component->end();?>