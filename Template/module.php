<?php

/**
 * Component name: Template
 * @author Grupa Leonardo <wpsupport@leonardo.pl>
 */

class Template extends Component
{
  public $name = 'Template';
  public $key = 'Template_component';
  public $label = 'Template component';

  public function config(): void
  {

    $key = $this->key;
    $fields = array();

    /**
     * Your magic goes here eq.:
     * $fields[] = ACFhelper::text($key . '_title', 'title', 'Title');
     *
     */

    $this->setFields($fields);
  }
}
