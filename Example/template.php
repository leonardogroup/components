<?php
$component->setTag('section');
$component->addClass('example-component');
$component->start();

?>
<div class="content typo">
  <?php if ($component->getField('title')) : ?>
    <h1><?php $component->theField('title'); ?></h1>
  <?php endif; ?>
  <?php $component->picture('image'); ?>
  <?php $component->theField('text'); ?>
  <?php
  $posts = $component->getField('object');

  if ($posts) :
    echo '<h3>My object: </h3>';
    echo '<ul>';
    foreach ($component->getField('object') as $myPost) :
      printf('<li><a href="%s" target="_blank">%s</a></li>', get_permalink($myPost->ID), $myPost->post_title);
    endforeach;
    echo '</ul>';
  endif;
  ?>
  <?php  ?>
  <?php foreach ($component->repeater('repeater2') as $key => $item) : ?>
    <?php
      $itemAtt = new AttributeControl;
      $itemAtt->addClass('item');
      $itemAtt->addClass('item-' . $key);
      ?>
    <article <?php $itemAtt->attributesHTML(); ?>>
      <h3><?= $item->theField('text'); ?></h3>
      <ul>
        <?php foreach ($item->repeater('repeater') as $sub) : ?>
          <li>
            <?php $sub->theField('text'); ?>
          </li>
        <?php endforeach; ?>
      </ul>
    </article>
  <?php endforeach; ?>
</div>
<?php $component->end(); ?>