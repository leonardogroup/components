<?php

class Example extends Component
{
  public $name = 'Example';
  public $key = 'Example_component';
  public $label = '📕 Example component';


  public function config(): void
  {
    $this->addSharedCss('typo');
    $this->addSharedJs('cookie');

    $key = $this->key;
    $fields = array();

    $fields[] = ACFhelper::text($key . '_title', 'title', 'Title');
    $fields[] = ACFhelper::image($key . '_image', 'image', 'Image');
    $fields[] = ACFhelper::rich($key . '_text', 'text', 'Text');

    $fields[] = ACFhelper::object($key . '_object', 'object', 'Object', ['page', 'post']);

    $fields[] = ACFhelper::repeater($key . '_repeater2', 'repeater2', 'Repeater 2', array(
      ACFhelper::text($key . '_repeater2_text', 'text', 'Text'),
      ACFhelper::repeater($key . '_repeater2_repeater', 'repeater', 'Repeater', array(
        ACFhelper::text($key . '_repeater2_repeater_text', 'text', 'Text')
      ))
    ));

    $this->setFields($fields);
  }
}
